# SV_PythonUebungen


Signalverarbeitung kann besonders gut gelernt und verstanden werden, wenn man die vorgestellten Verfahren in kleinen Programmierbeispielen selbstständig umsetzt. 
Die 4 wesentlichen Bibliotheken, die wir für die Signalverarbeitung benötigen sind 
`numpy`, `scipy.signal`, `matplotlib.pyplots` und `pandas`, wobei letztere eher der Datenauswertung in höheren Semestern dient.

Die Idee dieser Übungen ist es, allen Teilnehmer_innen ein Erfolgserlebnis zu vermitteln. Deshalb wird es immer drei Ebenen geben, die sich durch beschreibende Adjektive darstellen lassen: 

* Fluent / Sicher (F): Für diejenigen, die gerne selber knobeln und schon sicher darin sind, wie man Hilfe durch Stack-Overflow organisiert. Auf dieser Ebene werden nur die Ziele als Aufgaben definiert.

* Competent / Kompetent  (C): Für diejenigen, die ein Grundverständnis von Python bereits haben und leicht vorzerlegte Aufgaben eigenständig lösen können.

* Learning / Lernend (L): Für diejenigen, die etwas mehr Hilfe benötigen, um die Anfangssschritte aufzusetzen. Die Aufgaben werden in kleine Schritte zerlegt und es werden viele Tipps für die Hilfe zur Selbsthilfe gegeben.

Fangen Sie immer bei Sicher (F) an und springen Sie für jede Teilaufgabe immer dahin, wo Sie sich zur Lösung wohlfühlen (Lesen Sie aber immer zunächst den höheren Level durch). Manchmal sind Teilaufgaben evtl. lösbar, deshalb immer wieder nach oben springen. Hierfür sind jeweils Links vorgesehen.

Ich bin sicher, am Ende aller Übungen sind alle mindestens auf dem Competent-Level.

## Voraussetzungen: 

Ein funktionierendes Python System (3.8+) sollte vorliegen (besser 3.10+). Empfohlen wird das Standardpython für die einzelnen Plattformen (Win, Mac, Linux) und Visual Studio Code als Editor. Ob Sie Jupyter Notebooks oder Skripte verwenden, ist egal. Wenn Sie Skripte verwenden, achten Sie darauf, dass Ihre Python Version mit Tcl/Tk (tkinter) installiert wurde (einfaches GUI System). Test durch `python -m tkinter`.

## Hinweise zum guten Programmieren von Anfang an

* Vermeiden Sie magic numbers. Dies ist am einfachsten, wenn man bei keinem Funktionsaufruf feste Werte einträgt, sondern immer vorher eine Variable definiert. Bsp `fs = 16000` oder `sample_rate_Hz = 16000` (more pythonic) und dann diese Variable konsequent nutzt.

* Kommentieren Sie den Code, wo es notwendig ist.

## Inhalt
Dieses repository beinhaltet die MarkDown files. GitLab rendert diese Dateien bei einem Aufruf direkt, so dass alle Sprünge funktionieren.
